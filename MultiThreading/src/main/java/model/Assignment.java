package model;

/**
 * Created by Healeru on 3/11/2017.
 */
public class Assignment {
    private int _problemId;
    private long _grade;

    public Assignment(int problemId){
        _problemId = problemId;

    }

    public int getProblemId(){
        return _problemId;
    }

    public void setGrade(long grade){
        _grade = grade;
    }
    public long getGrade(){
        return _grade;
    }
}
