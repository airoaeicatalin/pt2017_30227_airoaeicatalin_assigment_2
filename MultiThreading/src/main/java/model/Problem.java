package model;

/**
 * Created by Healeru on 3/11/2017.
 */
public class Problem extends BaseEntity<Integer> {
    private String _description;

    public Problem(String description){
        _description = description;
    }

    public String getDescription(){
        return _description;
    }

    public void setDescription(String description){
        _description = description;
    }

    @Override
    public int hashCode(){
        int result = _description.hashCode();
        return result * 31;
    }

    @Override
    public boolean equals(Object other){
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;

        Problem p = (Problem)other;
        return _description != p.getDescription();
    }

    @Override
    public String toString(){
        return "{Problem (Description :" + _description + ")}-"
                + super.toString();
    }
}
