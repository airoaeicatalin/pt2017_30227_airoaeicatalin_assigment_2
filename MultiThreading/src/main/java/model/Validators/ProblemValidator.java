package model.Validators;

import model.Problem;

/**
 * Created by Healeru on 3/11/2017.
 */
public class ProblemValidator implements Validator<Problem> {
    public void validate(Problem problem) throws ProblemException{
        if (problem.getDescription().isEmpty())
            throw new ProblemException("The description must not be empty!");
    }
}
