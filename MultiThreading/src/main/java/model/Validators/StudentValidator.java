package model.Validators;

import model.Student;

/**
 * Created by Healeru on 3/11/2017.
 */
public class StudentValidator implements Validator<Student> {

    @Override
    public void validate(Student student) throws StudentException{
        String exception = "";
        if (!Character.isUpperCase(student.getName().charAt(0)))
            exception += "\nThe name must start with a capital letter!";
        String DOB = student.getDOB();
        String[] parts = DOB.split("/");
        if (parts.length != 3) {
            exception += "\nThe DOB format is invalid! Please use {mm/dd/yyyy}!";
            throw new StudentException(exception);
        }
        int month = Integer.parseInt(parts[0]);
        int day = Integer.parseInt(parts[1]);
        int year = Integer.parseInt(parts[2]);
        if (month < 0 || month > 12)
            exception += "\nThe month of the DOB is invalid!";
        if (day < 0 || day > 31)
            exception += "\nThe day of the DOB is invalid!";
        if (year > 2017)
            //throw new StudentException("The student must not be from the future!");
            exception += "\nThe year of the DOB is invalid!";
        if (exception != "") throw new StudentException(exception);
    }
}
