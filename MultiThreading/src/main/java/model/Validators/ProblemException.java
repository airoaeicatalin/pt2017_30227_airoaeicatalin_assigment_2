package model.Validators;

/**
 * Created by Healeru on 3/11/2017.
 */
public class ProblemException extends ValidatorException {
    public ProblemException(String message){
        super(message);
    }
    public ProblemException(String message,Throwable cause){
        super(message, cause);
    }
    public ProblemException(Throwable cause){
        super(cause);
    }
    /////////////////
}
