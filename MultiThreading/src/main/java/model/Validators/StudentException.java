package model.Validators;

/**
 * Created by Healeru on 3/11/2017.
 */
public class StudentException extends ValidatorException {
    public StudentException(String message){
        super(message);
    }
    public StudentException(String message,Throwable cause){
        super(message, cause);
    }
    public StudentException(Throwable cause){
        super(cause);
    }
}
