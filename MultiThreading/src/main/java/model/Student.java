package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Healeru on 3/11/2017.
 */
public class Student extends BaseEntity<Integer> {
    private String _name;
    private String _date_of_birth; // eg: "04/23/2017" =>Otherwise we consider it invalid
    List<Assignment> _assignments;

    public Student(String name,String date_of_birth){
        _name = name;
        _date_of_birth = date_of_birth;
        _assignments = new ArrayList<>();
    }
    public Student(){

    }

    //getters
    public String getName(){
        return _name;
    }
    public String getDOB(){
        return _date_of_birth;
    }
    //TO DO: get for assingments
    public List<Assignment> getAssignments(){
        return _assignments;
    }
    public long getGrade(int problemId){
        return _assignments.stream()
                .filter(as -> as.getProblemId() == problemId)
        .findAny().get().getGrade();
    }


    //setters
    public void setName(String name){
        _name = name;
    }
    public void setDOB(String date_of_birth){
        _date_of_birth = date_of_birth;
    }
    public void setAssignment(int problemId){
        Assignment assignment = new Assignment(problemId);
        _assignments.add(assignment);
    }
    public void setGrade(int grade,int problemId){
       _assignments.stream()
               .filter(as -> as.getProblemId() == problemId)
               .forEach(as -> as.setGrade(grade));
    }

    @Override
    public int hashCode(){
        int result = _name.hashCode();
        result = 31 * result + _date_of_birth.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object other){
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;

        Student student = (Student)other;
        if (student.getDOB() != _date_of_birth) return false;
        return _name.equals(student.getName());
    }

    @Override
    public String toString(){
        return "{Student( Name: " + _name+" DOB: " + _date_of_birth + ")}-"
                + super.toString();
    }

}
