package utils;

/**
 * Created by Healeru on 3/14/2017.
 */
public class ProblemIdGenerator {
    private static int id = 0;
    public static int generate(){
        return ++id;
    }
}
