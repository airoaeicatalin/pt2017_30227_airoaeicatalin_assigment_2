package repository;

import model.Assignment;
import model.Problem;
import model.Student;
import model.Validators.Validator;
import model.Validators.ValidatorException;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by Healeru on 3/14/2017.
 */
public class ProblemFileRepository extends RepositoryInMemory<Integer,Problem> {
    private String _fileName;

    public ProblemFileRepository(Validator<Problem> validator, String fileName){
        super(validator);
        _fileName = fileName;
        loadData();
    }

    private void loadData(){
        Path path = Paths.get(_fileName);
        try{
            Files.lines(path).forEach(line ->{
                List<String> items = Arrays.asList(line.split(";"));

                Integer id = Integer.valueOf(items.get(0));
                String description = items.get(1);
                Problem problem = new Problem(description);
                problem.setId(id);
                try{
                    super.save(problem);
                }catch(ValidatorException e){
                    e.printStackTrace();
                }
            });
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }
    private void saveData(){
        Path path = Paths.get(_fileName);
        try(BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.CREATE)){
            for (Map.Entry<Integer,Problem> entry : _entities.entrySet()){
                String line = entry.getKey() + ";" + entry.getValue().getDescription();
                bufferedWriter.write(line);
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Problem> save(Problem entity) throws ValidatorException{
        Optional<Problem> optional = super.save(entity);
        if (optional.isPresent()){
            return optional;
        }
        saveData();
        return Optional.empty();
    }

    @Override
    public Optional<Problem> delete(Integer id) {
        if (id == null){
            throw new IllegalArgumentException("Id must not be null!");
        }
        Optional<Problem> optional = Optional.ofNullable(_entities.remove(id));
        saveData();
        return optional;
    }

    @Override
    public Optional<Problem> update(Problem entity) throws ValidatorException {
        if (entity == null){
            throw new IllegalArgumentException("Entity must not be null!");
        }
        _validator.validate(entity);
        Optional<Problem> optional = Optional.ofNullable(_entities.computeIfPresent(entity.getId(),(k,v) -> entity));
        saveData();
        return optional;
    }
}
