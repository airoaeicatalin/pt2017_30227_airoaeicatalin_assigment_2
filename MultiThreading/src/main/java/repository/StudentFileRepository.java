package repository;

import model.Assignment;
import model.Student;
import model.Validators.Validator;
import model.Validators.ValidatorException;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

/**
 * Created by Healeru on 3/14/2017.
 */
public class StudentFileRepository extends RepositoryInMemory<Integer, Student> {
    private String _fileName;

    public StudentFileRepository(Validator<Student> validator,String fileName){
        super(validator);
        _fileName = fileName;
        loadData();
    }

    private void loadData(){
        Path path = Paths.get(_fileName);
        try{
            Files.lines(path).forEach(line ->{
                List<String> items = Arrays.asList(line.split(";"));

                Integer id = Integer.valueOf(items.get(0));
                String name = items.get(1);
                String DOB = items.get(2);
                Student student = new Student(name,DOB);
                student.setId(id);
                for(int i = 2; i < items.size(); ++i){
                    Integer pid = Integer.valueOf(items.get(i));
                    student.setAssignment(pid);
                }
                try{
                    super.save(student);
                }catch(ValidatorException e){
                    e.printStackTrace();
                }
            });
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }
    private void saveData(){
        Path path = Paths.get(_fileName);
        try(BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.CREATE)){
            for (Map.Entry<Integer,Student> entry : _entities.entrySet()){
                String line = entry.getKey() + ";" + entry.getValue().getName()
                        + ";" + entry.getValue().getDOB();
                List<Assignment> as = entry.getValue().getAssignments();
                String line2 = "";
                for (Assignment a : as){
                    line2 +=";" + a.getProblemId();
                }
                line += line2;
                bufferedWriter.write(line);
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Student> save(Student entity) throws ValidatorException{
        Optional<Student> optional = super.save(entity);
        if (optional.isPresent()){
            return optional;
        }
        saveData();
        return Optional.empty();
    }

    @Override
    public Optional<Student> delete(Integer id) {
        if (id == null){
            throw new IllegalArgumentException("Id must not be null!");
        }
        Optional<Student> optional = Optional.ofNullable(_entities.remove(id));
        saveData();
        return optional;
    }

    @Override
    public Optional<Student> update(Student entity) throws ValidatorException {
        if (entity == null){
            throw new IllegalArgumentException("Entity must not be null!");
        }
        _validator.validate(entity);
        Optional<Student> optional = Optional.ofNullable(_entities.computeIfPresent(entity.getId(),(k,v) -> entity));
        saveData();
        return optional;
    }

}
