package repository;

import model.Assignment;
import model.Student;
import model.Validators.Validator;
import model.Validators.ValidatorException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * Created by Healeru on 3/14/2017.
 */
public class XMLStudentRepository extends RepositoryInMemory<Integer, Student>{
    private String _fileName;

    public XMLStudentRepository(Validator<Student> validator, String fileName){
        super(validator);
        _fileName = fileName;
        try {
            loadData();
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Student> save(Student entity) throws ValidatorException {
        Optional<Student> optional = super.save(entity);
        if (optional.isPresent()){
            return optional;
        }
        try {
            saveData();
        } catch (ParserConfigurationException | IOException | SAXException |TransformerException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<Student> delete(Integer id) {
        if (id == null){
            throw new IllegalArgumentException("Id must not be null!");
        }
        Optional<Student> optional = Optional.ofNullable(_entities.remove(id));
        try {
            saveData();
        } catch (ParserConfigurationException | IOException | SAXException |TransformerException e) {
            e.printStackTrace();
        }
        return optional;
    }

    @Override
    public Optional<Student> update(Student entity) throws ValidatorException {
        if (entity == null){
            throw new IllegalArgumentException("Entity must not be null!");
        }
        _validator.validate(entity);
        Optional<Student> optional = Optional.ofNullable(_entities.computeIfPresent(entity.getId(),(k,v) -> entity));
        try {
            saveData();
        } catch (ParserConfigurationException | IOException | SAXException |TransformerException e) {
            e.printStackTrace();
        }
        return optional;
    }





    private void loadData() throws ParserConfigurationException, IOException, SAXException {
        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(_fileName);
        Element root = document.getDocumentElement();
        NodeList nodes = root.getChildNodes();
        for (int i = 0; i < nodes.getLength(); ++i){
            Node node = nodes.item(i);
            if (node instanceof Element){
                Student student = createStudentFromNode(node);
                super.save(student);
            }
        }
    }

    private Student createStudentFromNode(Node node){
        Integer id = Integer.valueOf(((Element) node).getAttribute("Id"));
        String name = getTextContentByTagName((Element) node,"Name");
        String DOB = getTextContentByTagName((Element) node,"DOB");
        String asList = getTextContentByTagName((Element) node,"Assignments");
        String[] items = asList.substring(0,asList.length() - 1).split(";");

        Student st = new Student(name,DOB);
        st.setId(id);
        for (String s : items){
            st.setAssignment(Integer.valueOf(s));
        }
        return st;
    }

    private String getTextContentByTagName(Element node,String tagName){
        NodeList nodes = node.getElementsByTagName(tagName);
        Node nodeWithTextContent = nodes.item(0);
        String textContent = nodeWithTextContent.getTextContent();
        return textContent;
    }

    private void saveData() throws ParserConfigurationException, IOException, SAXException, TransformerException {
        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(_fileName);
        Element root = document.getDocumentElement();

        for (Map.Entry<Integer,Student> entry : _entities.entrySet()){
            Node StudentNode = createStudentNode(document,root,entry.getValue());
            root.appendChild(StudentNode);
        }
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(new DOMSource(document),new StreamResult(new File(_fileName)));
    }

    private Node createStudentNode(Document document, Element root,Student student){
        Node StudentNode = document.createElement("Student");
        ((Element) StudentNode).setAttribute("Id", String.valueOf(student.getId()));
        appendChildNodeWithTextContent(document,StudentNode,"Name",student.getName());
        appendChildNodeWithTextContent(document,StudentNode,"DOB",student.getDOB());
        List<Assignment> all = student.getAssignments();
        String str ="";
        for (Assignment a: all){
            str += a.getProblemId() + ";";
        }
        appendChildNodeWithTextContent(document,StudentNode,"Assignments",str);

        return StudentNode;
    }

    private void appendChildNodeWithTextContent(Document document, Node parent, String tagName, String text){
        Node node = document.createElement(tagName);
        node.setTextContent(text);

        parent.appendChild(node);
    }


}
