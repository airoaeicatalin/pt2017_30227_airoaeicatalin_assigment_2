package repository;

import model.BaseEntity;
import model.Validators.Validator;
import model.Validators.ValidatorException;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Healeru on 3/14/2017.
 */
public class RepositoryInMemory<ID,T extends BaseEntity<ID>> implements Repository<ID,T> {
    protected Map<ID,T> _entities;
    protected Validator<T> _validator;

    public RepositoryInMemory(Validator<T> validator){
        _validator = validator;
        _entities = new HashMap<>();
    }

    @Override
    public Optional<T> findOne(ID id) {
        if (id == null){
            throw new IllegalArgumentException("Id must not be null!");
        }
        return Optional.ofNullable(_entities.get(id));
    }

    @Override
    public Iterable<T> findAll() {
        Set<T> All = _entities.entrySet()
                .stream()
                .map(entry -> entry.getValue())
                .collect(Collectors.toSet());
        return All;
    }

    @Override
    public Optional<T> save(T entity) throws ValidatorException {
        if (entity == null) {
            throw new IllegalArgumentException("Entity must not be null!");

        }
        _validator.validate(entity);
        return Optional.ofNullable(_entities.putIfAbsent(entity.getId(), entity));
    }

    @Override
    public Optional<T> delete(ID id) {
        if (id == null){
            throw new IllegalArgumentException("Id must not be null!");
        }
        return Optional.ofNullable(_entities.remove(id));
    }

    @Override
    public Optional<T> update(T entity) throws ValidatorException {
        if (entity == null){
            throw new IllegalArgumentException("Entity must not be null!");
        }
        _validator.validate(entity);
        return Optional.ofNullable(_entities.computeIfPresent(entity.getId(),(k,v) -> entity));
    }
}
