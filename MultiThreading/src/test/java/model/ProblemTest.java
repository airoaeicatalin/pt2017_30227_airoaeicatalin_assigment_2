package model;

import model.Validators.ProblemException;
import model.Validators.ProblemValidator;
import model.Validators.Validator;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Healeru on 3/11/2017.
 */
public class ProblemTest {

    @Test
    public void test(){
        Problem problem = new Problem("abcdefg");
        assertEquals(problem.getDescription(),"abcdefg");

        problem.setDescription("");
        Validator<Problem> validator = new ProblemValidator();
        try{
            validator.validate(problem);
            assert false;
        }catch(ProblemException ps){
            //System.out.println(ps.getMessage());
            assert true;
        }
    }
}