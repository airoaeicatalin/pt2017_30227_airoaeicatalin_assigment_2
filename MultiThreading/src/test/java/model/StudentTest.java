package model;

import model.Validators.StudentException;
import model.Validators.StudentValidator;
import model.Validators.Validator;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.*;

/**
 * Created by Healeru on 3/11/2017.
 */
public class StudentTest {

    @Test
    public void test(){
        Student student = new Student("Daniel","3/8/2012");
        student.setId(1);
        Validator<Student> validator = new StudentValidator();
        try{
            validator.validate(student);
            assert true;
        }catch(Exception e){
            assert false;
        }

        assertEquals(student.getName(),"Daniel");
        assertEquals(student.getDOB(),"3/8/2012");
        assertEquals(student.getAssignments(), Collections.emptyList());

        Student student1 = new Student("daniel","3/8/2012");
        try{
            validator.validate(student1);
            assert false;
        }catch(StudentException s1){
            //System.out.println(s1.getMessage());
            assert true;
        }
        Student student2 = new Student("Daniel","13/2/2012");
        try{
            validator.validate(student2);
            assert false;
        }catch(StudentException s1){
            //System.out.println(s1.getMessage());
            assert true;
        }
        Student student3 = new Student("Daniel","5/32/2018");
        try{
            validator.validate(student3);
            assert false;
        }catch(StudentException s1){
            //System.out.println(s1.getMessage());
            assert true;
        }
        Student student4 = new Student("Daniel","5/31/2018");
        try{
            validator.validate(student4);
            assert false;
        }catch(StudentException s1){
            //System.out.println(s1.getMessage());
            assert true;
        }

        //Assignment grading mechanism
        Problem problem = new Problem("blablabla");
        problem.setId(1);

        student.setAssignment(1);
        assertEquals(student.getAssignments().size(),1);
        assertEquals(student.getGrade(1),0);
        student.setGrade(5,1);
        assertEquals(student.getGrade(1),5);

        Problem problem1 = new Problem("fagagaga");
        problem1.setId(2);
        student.setAssignment(2);

        assertEquals(student.getAssignments().size(),2);
        assertEquals(student.getGrade(2),0);

        student.setGrade(10,2);

        assertEquals(student.getGrade(2),10);
    }
}