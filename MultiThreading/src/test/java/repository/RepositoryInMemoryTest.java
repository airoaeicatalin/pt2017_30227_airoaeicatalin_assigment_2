package repository;

import model.Problem;
import model.Student;
import model.Validators.ProblemValidator;
import model.Validators.StudentValidator;
import model.Validators.Validator;
import model.Validators.ValidatorException;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Healeru on 3/14/2017.
 */
public class RepositoryInMemoryTest {

    @Test
    public void test(){
        Student student = new Student("Daniel","3/8/2012");
        student.setId(1);
        Validator<Student> validator = new StudentValidator();
        Repository<Integer,Student> repo = new RepositoryInMemory<Integer,Student>(validator);
        repo.save(student);
        Student student1 = new Student("DAN","3/7/2012");
        student1.setId(2);
        repo.save(student1);
        Iterable<Student> it = repo.findAll();
        int id = 0;
        for (Student st : it){
            id++;
            assertEquals((Integer)st.getId(),(Integer)id);
        }
        Student student2 = new Student("Daniel","3/6/2013");
        student2.setId(2);
        repo.update(student2);
        Iterable<Student> it2 = repo.findAll();
        for(Student st : it2){
            assertEquals(st.getName(),"Daniel");
        }
        Student nullST = null;
        try{
            repo.save(nullST);
            assert false;
        }catch(IllegalArgumentException e){
            assert true;
        }
        Student st = new Student("aaa","aaa");
        try{
            repo.save(st);
            assert false;
        }catch (ValidatorException v){
            assert true;
        }

        Problem p1 = new Problem("afgagag");
        p1.setId(1);
        Validator<Problem> validator1 = new ProblemValidator();
        Repository<Integer,Problem> repo2 = new RepositoryInMemory<Integer, Problem>(validator1);

        repo2.save(p1);
        Iterable<Problem> ip = repo2.findAll();
        for(Problem p : ip){
            assertEquals(p.getDescription(),"afgagag");
        }
        try{
            repo2.save(null);
            assert false;
        }catch(IllegalArgumentException e){
            assert true;
        }

    }

}