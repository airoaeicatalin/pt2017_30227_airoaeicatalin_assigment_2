
import repository.RepositoryInMemoryTest;
import model.ProblemTest;
import model.StudentTest;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 * Created by Healeru on 3/11/2017.
 */
public class TestMain {
    public static void main(String[] args){
        Result result = JUnitCore.runClasses(StudentTest.class, ProblemTest.class, RepositoryInMemoryTest.class);
        for (Failure failure : result.getFailures() ){
            System.out.println(failure.toString());
        }
        System.out.println(result.wasSuccessful());
    }
}
